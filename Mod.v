`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.
//
// �013 - For Hell MicroFPGA by Hell Prototypes <pajoke@163.com>
//
////////////////////////////////////////////////////////////////////////////////

//**************************************************************************************************
// Synchronizes a HostIoRam read or write control signal to the clock domain of the memory device.
//**************************************************************************************************

module RamCtrlSync (clk_i, ctrlIn_i, ctrlOut_o, opBegun_i, doneIn_i, doneOut_o);

   input       clk_i;            // Clock from RAM domain.
   input       ctrlIn_i;         // Control signal from JTAG domain.
   output reg  ctrlOut_o;        // Control signal to RAM domain.
   input       opBegun_i;        // R/W operation begun signal from RAM domain.
   input       doneIn_i;         // R/W operation done signal from RAM domain.
   output reg  doneOut_o;        // R/W operation done signal to the JTAG domain.

   wire        ctrlIn;           // JTAG domain control signal sync'ed to RAM domain.

   // Sync the RAM control signal from the JTAG clock domain to the RAM domain.
   SyncToClock sync
   (
      .clk_i(clk_i),
      .unsynced_i(ctrlIn_i),
      .synced_o(ctrlIn)
   );

   // Now raise-and-hold the output control signal to the RAM upon a rising edge of the input control signal.
   // Lower the output control signal if the input control signal goes low or if the RAM signals that the
   // operation has begun or has finished.

   reg prevCtrlIn_v = 1;

   always @(posedge clk_i) begin
      if (ctrlIn == 0) begin
         // Lower the RAM control signal if the input signal has been deactivated.
         ctrlOut_o <= 0;
      end else if (prevCtrlIn_v == 0) begin
         // Raise the RAM control signal upon a rising edge of the input control signal.
         ctrlOut_o <= 1;
      end else if (opBegun_i == 1 || doneIn_i == 1) begin
         // Lower the RAM control signal once the RAM has begun or completed the R/W operation.
         ctrlOut_o <= 0;
      end
      prevCtrlIn_v <= ctrlIn; // Store the previous value of the input control signal.
   end

   // Inform the HostIoRam when the memory operation is done. Latch the done signal
   // from the RAM until the HostIoRam sees it and lowers its control signal.
   // Once the control signal is lowered, the RAM will eventually lower its done signal.
   always @(posedge clk_i) begin
      if (ctrlIn == 0) begin
         doneOut_o <= 0;
      end else if (doneIn_i == 1) begin
         doneOut_o <= 1;
      end
   end
endmodule


//Reset generater
module reset (input CLK, input nHold, output nRst);
reg count;
assign nRst = count;
always @(posedge CLK)  begin
	if( nHold ) begin
		if (!nRst) begin
			count <= 1'b1;
		end
	end else begin
		count <= 1'b0;
	end
end
endmodule
