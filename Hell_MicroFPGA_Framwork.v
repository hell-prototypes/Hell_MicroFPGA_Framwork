`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.
//
// �013 - For Hell MicroFPGA by Hell Prototypes <pajoke@163.com>
//
////////////////////////////////////////////////////////////////////////////////
`define CLK_MUL		3 //36M
`define CLK_DIV		1
//**************************************************************************************************
// Setups SDRAM <-> PC communication. Application on the PC side can perform tests by writing and
// reading back from the SDRAM memory.
//**************************************************************************************************
module Hell_MicroFPGA_Framwork (
	PIC_SCK, PIC_MOSI, PIC_MISO, ROM_MISO,ROM_SCK, ROM_MOSI, ROM_CSO_B, IO_Port_o,
	clk_i, sdClk_o, sdCe_bo, sdRas_bo, sdCas_bo, sdWe_bo, sdBs_o, sdAddr_o, sdData_io, sdClkFb_i,
	key_0_i,key_1_i,key_2_i,key_3_i
);

parameter  SADDR_WIDTH  = 12;                // SDRAM-side address width.
parameter  DATA_WIDTH   = 16;                // Host & SDRAM data width.

input  ROM_MISO;
output ROM_SCK;
output ROM_MOSI;
output ROM_CSO_B;

assign ROM_MOSI = ROM_MISO;
assign ROM_SCK = 1'b0;
assign ROM_CSO_B = 1'b1;

input  PIC_SCK, PIC_MOSI;
output PIC_MISO;
output [19:0] IO_Port_o;

input key_0_i;
input key_1_i;
input key_2_i;
input key_3_i;

output      sdCe_bo;						 // Chip-select to SDRAM.
output      sdRas_bo;                        // SDRAM row address strobe.
output      sdCas_bo;                        // SDRAM column address strobe.
output      sdWe_bo;                         // SDRAM write enable.
output      [1:0] sdBs_o;                    // SDRAM bank address.
output      [SADDR_WIDTH-1:0] sdAddr_o;      // SDRAM row/column address.
inout       [DATA_WIDTH-1:0] sdData_io;      // Data to/from SDRAM.
input       sdClkFb_i;                   
output      sdClk_o;
input       clk_i;
//========================================================= 
wire        wrDone;
wire        rdDone;

wire        [21:0] sdraddr;
wire        [15:0] datai;
wire        [15:0] datao;

wire        wrCtrl;
wire        rdCtrl;
wire        wrSdram;
wire        rdSdram;

wire        rwDone_s;
wire        rdDone_s;
wire        wrDone_s;

wire        opBegun_o;

wire        nHold;

wire        Test_IO_Mode;
wire        io_test_i = |IO_Port_o;
wire        LED_Out;
wire		key = key_0_i & key_1_i & key_2_i & key_3_i;
wire        LED = Test_IO_Mode  ? (~key) :  LED_Out;
assign		IO_Port_o[19:0] = {
	key, ~key,~key,  key, 
	key, ~key, key, ~key, 
	key, ~key, key, ~key,
	key, ~key, key, ~key,
	LED,  key, ~key, key 
};

//========================================================= 
// Generate ??MHz clock and feed it to SDRAM.
ClkGen #(.MUL(`CLK_MUL), .DIV(`CLK_DIV), .IN_FREQ(12.0)) clkgen_inst(
	.clk_i(clk_i),
	.clk_o(sdClk_o),
	.locked(nHold)
);
wire clk_core = ~sdClkFb_i;
//========================================================= 
reset reset_inst(
	.CLK(clk_i),
	.nHold(nHold),
	.nRst(nRst)
);
//========================================================= 
COMM COMM_inst (
	.Clk_i(clk_core), .nRst_i(nRst), .io_test_i(io_test_i),
	.SCK(PIC_SCK), .MOSI(PIC_MOSI), .MISO(PIC_MISO), .Test_IO(Test_IO_Mode),
	.addr_o(sdraddr), .dataFromHost_o(datai),
	.dataToHost_i(datao),
	.wr_o(wrCtrl), .rd_o(rdCtrl), .rwDone_i(rwDone_s)
);
//========================================================= 
RamCtrlSync syncRead(
	.clk_i(clk_core),      // Clock from RAM domain.
	.ctrlIn_i(rdCtrl),      // Control signal from JTAG domain.
	.ctrlOut_o(rdSdram),    // Control signal to RAM domain.
	.opBegun_i(opBegun_o),  // R/W operation begun signal from RAM domain.
	.doneIn_i(rdDone),      // R/W operation done signal from RAM domain.
	.doneOut_o(rdDone_s)    // R/W operation done signal to the JTAG domain.
);
//========================================================= 
RamCtrlSync syncWrite(
	.clk_i(clk_core),      // Clock from RAM domain.
	.ctrlIn_i(wrCtrl),      // Control signal from JTAG domain.
	.ctrlOut_o(wrSdram),    // Control signal to RAM domain.
	.opBegun_i(opBegun_o),  // R/W operation begun signal from RAM domain.
	.doneIn_i(wrDone),      // R/W operation done signal from RAM domain.
	.doneOut_o(wrDone_s)    // R/W operation done signal to the JTAG domain.
);

//========================================================= 
assign rwDone_s = rdDone_s | wrDone_s;


SdramCtrl #(.FREQ((`CLK_MUL * 12)/`CLK_DIV)) sdram (
	.clk_i(clk_core),
	.lock_i(1'b1),     
	.rst_i(~nRst),     
	.rd_i(rdSdram),    
	.wr_i(wrSdram),      
	.opBegun_o(opBegun_o),     
	.done_o(wrDone),     
	.rdDone_o(rdDone),
	.addr_i(sdraddr),       
	.data_i(datai),    
	.data_o(datao),
	.sdCe_bo(sdCe_bo),
	.sdRas_bo(sdRas_bo), 
	.sdCas_bo(sdCas_bo),
	.sdWe_bo(sdWe_bo), 
	.sdBs_o(sdBs_o),   
	.sdAddr_o(sdAddr_o), 
	.sdData_io(sdData_io)
);
//========================================================= 
Notify_light Notify_light_inst (
    .CLK(clk_i), 
    .nRst(nRst), 
    .enable(1), 
    .pwm_out(LED_Out)
);
endmodule

