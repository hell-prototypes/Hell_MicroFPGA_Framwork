`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.
//
// �013 - For Hell MicroFPGA by Hell Prototypes <pajoke@163.com>
//
////////////////////////////////////////////////////////////////////////////////

module Test_Bench( );

reg  clk_i;

reg PIC_SCK;
reg PIC_MOSI;
wire PIC_MISO;
wire ROM_MISO = 0;
wire ROM_SCK;
wire ROM_MOSI;
wire ROM_CSO_B;

reg  [15:0]sd_data;

wire sdClk_o;
wire sdRas_bo;
wire sdCas_bo;
wire sdCe_bo;
wire sdWe_bo;
wire [1:0]sdBs_o;
wire [11:0]sdAddr_o;
wire wr_en = !sdWe_bo && !sdCe_bo && !sdCas_bo && sdRas_bo && !sdAddr_o[10];
wire [15:0]sdData_io = ~wr_en ? sd_data : 16'hzzzz;
wire sdClkFb_i = sdClk_o;

always @(posedge sdClk_o) begin
	if(wr_en) begin
		sd_data = sdData_io;
	end
end
//=========================================================
initial begin
	clk_i=0;
	PIC_SCK = 0;
	PIC_MOSI = 0;
end
always begin
	#43; clk_i = 1;
	#40; clk_i = 0;
end
//=========================================================
reg wrbyte_req;
reg [7:0] wrbyte_data;

initial begin 
	wrbyte_req=0; 
	wrbyte_data=0; 
end

always @(posedge wrbyte_req) begin : temp
	integer i;
	i = 7;
	#100;

	repeat (8)  begin 
		PIC_SCK = 0; 
		PIC_MOSI = wrbyte_data[i]; 
		i=i-1;       #1000;
		PIC_SCK = 1; #1000;
	end
	PIC_SCK = 0;
	PIC_MOSI = 0;
	#1000;
	wrbyte_req = 0;
end
//=========================================================
task write_cmd;
input [7:0] value;
integer i;
begin 
	wrbyte_req = 1;
	wrbyte_data = value;
	@(negedge wrbyte_req);
end
endtask
//=========================================================
initial
begin
	#500000;
//==============================
	write_cmd (8'h80);
	write_cmd (8'h00);
	write_cmd (8'h00);

	#3000;
	write_cmd (8'h44);//Write
	write_cmd (8'h44);
	write_cmd (8'h55);
	write_cmd (8'h66);
	write_cmd (8'h77);

	#3000;
	write_cmd (8'h80);
	write_cmd (8'h00);
	write_cmd (8'h00);
	
	#3000;
	write_cmd (8'h04);//Read
	write_cmd (8'hff);
	write_cmd (8'h00);
	write_cmd (8'hff);
	write_cmd (8'h00);

	#500000;

	$finish;
end

Hell_MicroFPGA_Framwork Hell_MicroFPGA_Framwork_inst (
    .PIC_SCK(PIC_SCK), 
    .PIC_MOSI(PIC_MOSI), 
    .PIC_MISO(PIC_MISO), 
    .ROM_MISO(ROM_MISO), 
    .ROM_SCK(ROM_SCK), 
    .ROM_MOSI(ROM_MOSI), 
    .ROM_CSO_B(ROM_CSO_B), 
    .LED(), 
    .clk_i(clk_i), 
    .sdClk_o(sdClk_o), 
    .sdCe_bo(sdCe_bo), 
    .sdRas_bo(sdRas_bo), 
    .sdCas_bo(sdCas_bo), 
    .sdWe_bo(sdWe_bo), 
    .sdBs_o(sdBs_o), 
    .sdAddr_o(sdAddr_o), 
    .sdData_io(sdData_io), 
    .sdClkFb_i(sdClkFb_i)
);


endmodule
