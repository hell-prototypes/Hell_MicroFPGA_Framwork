`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.
//
// �013 - For Hell MicroFPGA by Hell Prototypes <pajoke@163.com>
//
////////////////////////////////////////////////////////////////////////////////
`define FW_VERSION			4'h1

//**************************************************************************************************
// 
//**************************************************************************************************
module COMM(
	Clk_i, nRst_i, SCK, MOSI, MISO, Test_IO, io_test_i,
	addr_o, dataFromHost_o, dataToHost_i, wr_o, rd_o, rwDone_i
);

input Clk_i;
input nRst_i;
input  SCK, MOSI;
output MISO;
output Test_IO;
input io_test_i;

output [15:0] dataFromHost_o;
output [22:0] addr_o;
input  [15:0] dataToHost_i;
output wr_o;
output rd_o;
input  rwDone_i;
//========================================================= 
parameter 
	READ_CMD    	= 3'b000,
	READ_SDRAM		= 3'b001,
	WRITE_SDRAM		= 3'b010,
	SET_ADDRESS		= 3'b011,
	IO_TEST 		= 3'b100;
//========================================================= 
reg [15:0] dataFromHost, next_dataFromHost;
reg [15:0] dataFromMem, next_dataFromMem;
reg [21:0] addrFromHost, next_addrFromHost;//4M Word
reg readFromMem, next_readFromMem;
reg writeToMem, next_writeToMem;
reg word_ready, next_word_ready;
reg feed_first_byte, next_feed_first_byte;

assign dataFromHost_o = dataFromHost;
assign addr_o = addrFromHost;
assign wr_o = writeToMem;
assign rd_o = readFromMem;
//========================================================= 
reg test_io_mode, next_test_io_mode;
assign Test_IO = test_io_mode;

//========================================================= 
reg [2:0] state, next_state;
reg  [4:0]byte_count, next_byte_count;
reg  SPI_last_BF, next_SPI_last_BF;
reg  SPI_Wr, next_SPI_Wr;
reg  [7:0]SPI_Byte_Out, next_SPI_Byte_Out;
wire BF_o;
wire [7:0]SPI_Byte_In;
SPI_Slave SPI_Slave_inst (
    .Clk_i(Clk_i), 
    .nRst_i(nRst_i), 
    .SCK(SCK), 
    .MOSI(MOSI), 
    .MISO(MISO), 
    .Buffer_o(SPI_Byte_In), 
    .BF_o(BF_o), 
    .Wr_i(SPI_Wr), 
    .Data_i(SPI_Byte_Out)
);
wire byte_ready = (~SPI_last_BF) & BF_o;
wire byte_count_zero = ~(|byte_count);
wire FPGA_Status = {4'ha, `FW_VERSION};
//=========================================================
always @(posedge Clk_i or negedge nRst_i) begin
	if (!nRst_i) begin
		state <= READ_CMD;
		SPI_Wr <= 1'b0;
		SPI_Byte_Out <= 8'h00;
		SPI_last_BF <= BF_o;
		byte_count <= 8'h00;
		test_io_mode <= 0;

		dataFromHost <= 16'h0000;
		dataFromMem <= 16'h0000;
		addrFromHost <= 23'h000000;
		readFromMem <= 1'b0;
		writeToMem <= 1'b0;
		word_ready <= 1'b0;
		feed_first_byte <= 1'b0;
	end else begin
		state <= next_state;
		SPI_Wr <= next_SPI_Wr;
		SPI_Byte_Out <= next_SPI_Byte_Out;
		SPI_last_BF <= next_SPI_last_BF;
		byte_count <= next_byte_count;
		test_io_mode <= next_test_io_mode;
		
		dataFromHost <= next_dataFromHost;
		dataFromMem <= next_dataFromMem;
		addrFromHost <= next_addrFromHost;
		readFromMem <= next_readFromMem;
		writeToMem <= next_writeToMem;
		word_ready <= next_word_ready;
		feed_first_byte <= next_feed_first_byte;
	end
end

always @* begin
	next_state = state;
	next_SPI_Wr = 1'b0;
	next_SPI_Byte_Out = SPI_Byte_Out;
	next_SPI_last_BF = BF_o;
	next_byte_count = byte_count;
	next_test_io_mode = test_io_mode;

	next_dataFromHost = dataFromHost;
	next_dataFromMem  = dataFromMem;
	next_addrFromHost = addrFromHost;
	next_readFromMem  = readFromMem;
	next_writeToMem   = writeToMem;
	next_word_ready   = word_ready;
	next_feed_first_byte = feed_first_byte;

	case (state)
		READ_CMD : begin
			if (byte_ready) begin
				if (~SPI_Byte_In[7]) begin //0: for command
					if(SPI_Byte_In[6]) begin //rd/wr sdram 
						next_byte_count = SPI_Byte_In[4:0];
						next_word_ready = 1'b0;
						if(SPI_Byte_In[5]) begin //write data
							next_state = WRITE_SDRAM;
						end else begin           //read data
							next_feed_first_byte = 1'b0;
							next_state = READ_SDRAM;
						end
					end else begin//Extern command
						//IO test command
						next_byte_count = 5'h01;//read 3byte
						next_test_io_mode = SPI_Byte_In[0];
						next_feed_first_byte = 0;
						next_state = IO_TEST;
					end
				end else begin //1: for set sdram address
					next_addrFromHost[21:16] = SPI_Byte_In[5:0];
					next_byte_count = 6'h02;
					next_state = SET_ADDRESS;
				end
			end
		end
		READ_SDRAM : begin
			if (byte_ready) begin
				next_byte_count = byte_count - 1;
				if(!byte_count[0]) begin
					next_SPI_Byte_Out = dataFromMem[7:0];
					next_SPI_Wr = 1'b1;
					next_word_ready = 1'b0;
				end
			end

			if((!word_ready) && (!byte_count[0])) begin
				if ( readFromMem & rwDone_i ) begin
					next_readFromMem = 0;
					next_dataFromMem = dataToHost_i;
					next_addrFromHost = addrFromHost + 1;
					next_word_ready = 1'b1;
					next_feed_first_byte = 1'b0;
				end else if (~byte_count_zero) begin
					next_readFromMem = 1'b1;
				end
			end else begin
				if(!feed_first_byte) begin
					next_feed_first_byte = 1'b1;
					next_SPI_Byte_Out = dataFromMem[15:8];
					next_SPI_Wr = 1'b1;
				end
			end

			if(byte_count_zero) begin
				next_SPI_Byte_Out = FPGA_Status;
				next_SPI_Wr = 1'b1;
				next_state = READ_CMD;
			end
		end
		WRITE_SDRAM : begin
			if (byte_ready) begin
				next_byte_count = byte_count - 1;
				if(byte_count[0]) begin
					next_dataFromHost[7:0] = SPI_Byte_In;
					next_word_ready = 1'b1;
				end else begin
					next_dataFromHost[15:8] = SPI_Byte_In;
				end
			end
			if(word_ready) begin
				next_word_ready = 1'b0;
				next_writeToMem = 1'b1;
			end
			if ( writeToMem & rwDone_i ) begin
				next_writeToMem = 1'b0;
				next_addrFromHost = addrFromHost + 1;
				if(byte_count_zero) begin
					next_SPI_Byte_Out = FPGA_Status;
					next_SPI_Wr = 1'b1;
					next_state = READ_CMD;
				end
			end
		end
		SET_ADDRESS : begin
			if (byte_ready) begin
				next_byte_count = byte_count - 1;
				if(byte_count[0]) begin
					next_addrFromHost[7:0] = SPI_Byte_In;
				end else begin
					next_addrFromHost[15:8] = SPI_Byte_In;
				end
			end
			if(byte_count_zero) begin
				next_SPI_Byte_Out = FPGA_Status;
				next_SPI_Wr = 1'b1;
				next_state = READ_CMD;
			end
		end
		IO_TEST : begin
			if (byte_ready) begin
				next_byte_count = byte_count - 1;
			end
			if(!feed_first_byte) begin
				next_feed_first_byte = 1'b1;
				next_SPI_Byte_Out = {7'h00, io_test_i}; //set feed back value
				next_SPI_Wr = 1'b1;
			end
			if(byte_count_zero) begin
				next_SPI_Byte_Out = FPGA_Status;
				next_SPI_Wr = 1'b1;
				next_state = READ_CMD;
			end
		end
	endcase
end

endmodule
