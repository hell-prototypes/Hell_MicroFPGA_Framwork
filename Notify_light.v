`timescale 1ns / 1ps
////////////////////////////////////////////////////////////////////////////////
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.
//
// 婕013 - For Hell MicroFPGA by Hell Prototypes <pajoke@163.com>
//
////////////////////////////////////////////////////////////////////////////////
module Notify_light(
	input  CLK,
	input  nRst,

	input  enable,

	output reg pwm_out
);

reg [7:0] PWM_Tab[0:63];
reg [5:0] PWM_Tab_index;
wire [7:0] PWM_Value = PWM_Tab[PWM_Tab_index];

`define init_2(b0,b1) PWM_Tab[i]=b0; PWM_Tab[i+1]=b1; i=i+2
`define init_4(b0,b1,b2,b3) `init_2(b0, b1); `init_2(b2,b3)
`define init_8(b0,b1,b2,b3,b4,b5,b6,b7) `init_4(b0,b1,b2,b3); `init_4(b4,b5,b6,b7)

integer i;
initial begin
	i=0;
	`init_8(1  , 5  , 11 , 15 , 20 , 25 , 30 , 36);
	`init_8(43 , 49 , 56 , 64 , 72 , 88 , 97 , 105);
	`init_8(114, 132, 141, 150, 158, 167, 183, 206);
	`init_8(212, 225, 230, 235, 240, 245, 250, 254);
	`init_8(250, 244, 240, 235, 225, 219, 212, 206);
	`init_8(191, 183, 175, 167, 150, 141, 132, 123);
	`init_8(114, 105, 97 , 88 , 72 , 64 , 56 , 49);
	`init_8(36 , 30 , 25 , 20 , 15 , 11 , 5  , 0);
end

//=========================================================
reg [7:0] pwm_count;
reg [5:0] tick;
reg [11:0] divider;
`define DIV_TOP		12'd800

always @(posedge CLK or negedge nRst) begin
	if (!nRst) begin
		PWM_Tab_index <= 0;
		pwm_count <= 0;
		tick <= 0;
		divider <= 0;
	end else begin
		if(enable) begin
			tick <= tick + 1;
			if (&tick) begin
				pwm_count <= pwm_count +1;
				 if(&pwm_count)begin
					pwm_out <= 1'b0;
				end else if(pwm_count == PWM_Value) begin
					pwm_out <= 1'b1;
				end 

				if(divider == `DIV_TOP) begin
					divider <= 0;
					PWM_Tab_index <= PWM_Tab_index + 1;
				end else begin
					divider <= divider + 1;
				end
			end
		end else begin
			pwm_out <= 0;
		end
	end
end
endmodule
