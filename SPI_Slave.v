////////////////////////////////////////////////////////////////////////////////
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.
//
// �013 - For Hell MicroFPGA by Hell Prototypes <pajoke@163.com>
//
////////////////////////////////////////////////////////////////////////////////

module SPI_Slave( Clk_i, nRst_i, SCK, MOSI, MISO, Buffer_o, BF_o, Wr_i, Data_i );

input Clk_i;
input nRst_i;
input  SCK, MOSI;
output MISO;

output [7:0] Buffer_o; //Data buffer
output BF_o;           //Buffer full flag

input  [7:0] Data_i;
input  Wr_i;
//========================================================= 
reg [1:0] SCK_r;
reg [2:0] bit_count;
reg [7:0] SPI_SR;
reg [7:0] Buffer_o;
reg BF;
reg MOSI_r;

wire bit_count_zero = ~(|bit_count);
assign MISO = SPI_SR[7];
assign BF_o = BF & (~Wr_i);
//========================================================= 
wire SCK_POS = SCK_r == 2'b01;
wire SCK_NEG = SCK_r == 2'b10;
//=========================================================
always @(posedge Clk_i or negedge nRst_i) begin
	if (!nRst_i) begin
		bit_count <= 0;
		SPI_SR <= 0;
		SCK_r <= {SCK, SCK};
		BF <= 0;
		MOSI_r <= 0;
	end else begin
		SCK_r <= {SCK_r[0], SCK};

		if(SCK_POS) begin
			MOSI_r <= MOSI;
			bit_count <= bit_count + 1;
		end
		
		if(SCK_NEG) begin
			SPI_SR <= {SPI_SR[6:0], MOSI_r};
			if(bit_count_zero) begin
				BF <= 1;
				Buffer_o <= {SPI_SR[6:0], MOSI_r};
			end
		end

		if(bit_count_zero) begin
			if(Wr_i) begin
				SPI_SR <= Data_i;
				BF <= 0;
			end
		end else begin
			BF <= 0;
		end
	end
end

endmodule
